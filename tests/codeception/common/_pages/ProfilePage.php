<?php

namespace tests\codeception\common\_pages;

use yii\codeception\BasePage;

/**
 * Represents loging page
 * @property \codeception_frontend\AcceptanceTester|\codeception_frontend\FunctionalTester|\codeception_backend\AcceptanceTester|\codeception_backend\FunctionalTester $actor
 */
class ProfilePage extends BasePage
{
    public $route = 'user/profile';

    /**
     * @param string $user_full_name
     * @param string $user_address
     */
    public function fill_profile_fields($user_full_name, $user_address)
    {
        $this->actor->click('#edit_profile');
        $this->actor->fillField('input[name="UserProfile[user_full_name]"]', $user_full_name);
        $this->actor->fillField('[name="UserProfile[user_address]"]', $user_address);
        $this->actor->click('.update-user');
    }
    /**
     * Empty fields
     */
    public function empty_profile_fields()
    {
        $this->actor->click('#edit_profile');
        $this->actor->fillField('input[name="UserProfile[user_full_name]"]', '');
        $this->actor->fillField('[name="UserProfile[user_address]"]', '');
        $this->actor->click('.update-user');
    }
    /**
     * Empty fields
     */
    public function empty_setting_fields()
    {
        $this->actor->click('#settings');
        $this->actor->fillField('input[name="UserProfile[currentpassword]"]', '');
        $this->actor->fillField('input[name="UserProfile[password]"]', '');
        $this->actor->fillField('input[name="UserProfile[repeatpassword]"]', '');
        $this->actor->click('.update-user');
    }
    /**
     * @param string $wrong_old_pwd
     * @param string $new_pwd
     */
    public function fill_mismatch_old_pwd($wrong_old_pwd,$new_pwd)
    {
        $this->actor->click('#settings');
        $this->actor->fillField('input[name="UserProfile[currentpassword]"]', $wrong_old_pwd);
        $this->actor->fillField('input[name="UserProfile[password]"]', $new_pwd);
        $this->actor->fillField('input[name="UserProfile[repeatpassword]"]', $new_pwd);
        $this->actor->click('.update-user');
    }
    /**
     * @param string $correct_old_pwd
     * @param string $new_pwd
     * @param string $mismatch_new_pwd
     */
    public function fill_mismatch_new_pwd($correct_old_pwd,$new_pwd,$mismatch_new_pwd)
    {
        $this->actor->click('#settings');
        $this->actor->fillField('input[name="UserProfile[currentpassword]"]', $correct_old_pwd);
        $this->actor->fillField('input[name="UserProfile[password]"]', $new_pwd);
        $this->actor->fillField('input[name="UserProfile[repeatpassword]"]', $mismatch_new_pwd);
        $this->actor->click('.update-user');
    }
    /**
     * @param string $correct_old_pwd
     * @param string $new_pwd
     */
    public function fill_correct_pwd($correct_old_pwd,$new_pwd)
    {
        $this->actor->click('#settings');
        $this->actor->fillField('input[name="UserProfile[currentpassword]"]', $correct_old_pwd);
        $this->actor->fillField('input[name="UserProfile[password]"]', $new_pwd);
        $this->actor->fillField('input[name="UserProfile[repeatpassword]"]', $new_pwd);
        $this->actor->click('.update-user');
    }
}
