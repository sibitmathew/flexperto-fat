<?php

use tests\codeception\backend\AcceptanceTester;
use tests\codeception\common\_pages\LoginPage;

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure dashboard page works');

$loginPage = LoginPage::openBy($I);

$I->amGoingTo('try to login with correct credentials');
$loginPage->login('erau', 'password_0');
if (method_exists($I, 'wait')) {
    $I->wait(3); // only for selenium
}
$I->expectTo('see that user is logged');
$I->see('Dashboard');
$I->see('Users');

$I->click('Dashboard');
$I->see('No. of Total users');
$I->see('No. of Active users');
$I->see('No. of Non-active users');

$I->click('Users');
$I->see('Users list');

$I->dontSeeLink('Login');
$I->dontSeeLink('Signup');
/** Uncomment if using WebDriver
 * $I->click('Logout (erau)');
 * $I->dontSeeLink('Logout (erau)');
 * $I->seeLink('Login');
 */
