<?php
use tests\codeception\frontend\FunctionalTester;
use tests\codeception\common\_pages\LoginPage;
use tests\codeception\common\_pages\ProfilePage;

/* @var $scenario Codeception\Scenario */

$I = new FunctionalTester($scenario);
$I->wantTo('ensure profile page works');

$loginPage = LoginPage::openBy($I);
$profilePage = ProfilePage::openBy($I);



$I->amGoingTo('try to login with correct credentials');
$loginPage->login('erau', 'password_0');
$I->expectTo('see that user is logged');
$I->see('Logout (erau)', 'form button[type=submit]');
$I->dontSeeLink('Login');
$I->dontSeeLink('Signup');
$I->see('Profile');
$I->see('erau');
$I->see('Overview');


$I->amGoingTo('try to check Overview page');
$I->click('Overview');
$I->see('Edit user settings:');
$I->amGoingTo('try to check edit profile page');
$I->see('Edit Profile');
$I->click('Edit Profile');
$I->see('Create/Edit Users');

/* Use WebDriver*/
$I->amGoingTo('try to check validation is working with empty data');
$I->see('Edit Profile');
$I->click('Edit Profile');
$profilePage->empty_profile_fields();
$I->expectTo('see validations errors');
$I->see('User Full Name cannot be blank.', '.help-block');
$I->see('User Address cannot be blank.', '.help-block');

/* Use WebDriver*/
$I->amGoingTo('try to check with profile fields data');
$I->see('Edit Profile');
$I->click('Edit Profile');
$profilePage->fill_profile_fields('Erau','#10, CST ,IN');
$I->expectTo('success message');
$I->see('Successfully Updated!','#res_msg_update-profile');

/* Use WebDriver*/
$I->see('Avatar');
$I->click('Avatar');
$I->see('Upload Avatar');
$I->amGoingTo('try to check upload without image attachment');
$I->click('Upload');
$I->expectTo('see validations errors');
$I->see('Please select an image to upload!', '#res_msg_update-avatar');

/* Use WebDriver*/
$I->amGoingTo('try to check upload with image attachment');
$I->attachFile('input[@type="file"]', 'testimage.jpg');
$I->click('Upload');
$I->expectTo('success message');
$I->see('Successfully uploaded!','#res_msg_update-avatar');

/* Use WebDriver*/
$I->see('Settings');
$I->click('Settings');
$I->see('Account cancellation/Change password');

/* Use WebDriver*/
$I->amGoingTo('try to check with empty password fields');
$profilePage->empty_setting_fields();
$I->expectTo('see validations errors');
$I->see('Currentpassword cannot be blank.', '.help-block');
$I->see('Password cannot be blank.', '.help-block');
$I->see('Repeatpassword cannot be blank.', '.help-block');

/* Use WebDriver*/
$I->amGoingTo('try to check with mismatch old password');
$profilePage->fill_mismatch_old_pwd('password_wrong','password_0');
$I->expectTo('see validations errors');
$I->see('Current password is incorrect.', '.help-block');

/* Use WebDriver*/
$I->amGoingTo('try to check with mismatch new password');
$profilePage->fill_mismatch_new_pwd('password_0','password_2','password_1');
$I->expectTo('see validations errors');
$I->see('Repeatpassword must be equal to "Password".', '.help-block');

/* Use WebDriver*/
$I->amGoingTo('try to check with mismatch new password');
$profilePage->fill_correct_pwd('password_0','password_1');
$I->expectTo('success message');
$I->see('Successfully changed password!', '#res_msg_update-password');



