/*Javascript file used for front end operations*/
$(document).ready(function(){
	$(".update-user").click(function(){
		var form = $(this).attr('data-form');
		update(form);
	});
	
/*To Save/Update users*/
	   function update(form){
   		$(document).off('submit', '#'+form).on('submit', '#'+form, function(e){
			var formObj = $(this);
			var formURL = formObj.attr("action");

			if(window.FormData !== undefined)  /* for HTML5 browsers */
				{
					var formData = new FormData(this);
					$.ajax({
			        	url: formURL,
				        type: 'POST',
						data:  formData,
						mimeType:"multipart/form-data",
						contentType: false,
			    	    cache: false,
			        	processData:false,
			        	dataType: 'json',
						success: function(data, textStatus, jqXHR)
					    {
								$(".help-block").remove();
	   	  					 	$(".form-group").removeClass("has-error");
								if(data.result=='error'&&data.type=='validation'){
									$.each(data.msg, function(key, val) {
							   	   		$("#"+key+"_err").remove();
					                    $("#"+key).after("<div class=\"help-block\" id='"+key+"_err'>"+val+"</div>");
					                    $("#"+key).closest(".form-group").addClass("has-error");
					                });
								}
								if(data.result=='success'){
									$('#res_msg_'+form).show();
									$('#res_msg_'+form).css({"color":"#075617"});
									$('#res_msg_'+form).text(data.msg);
									if(typeof(data.img) != 'undefined'&&data.img != ''){
										$("#user_img").attr('src','../web/uploads/'+data.img);
									}
									if(typeof(data.details)!= 'undefined'&&data.details != ''){
										$('#user_full_name_view').text(data.details['user_full_name']);
										$('#user_age_view').text(data.details['user_age']);
										$('#contact_no_view').text(data.details['contact_no']);
										$('#user_address_view').text(data.details['user_address']);
										$('#user_name').text(data.details['user_full_name']);
									}
									setTimeout(function(){ 
										$('#res_msg_'+form).hide(); 
									}, 1000);
								}
								if(data.result=='error'&&data.type=='other'){
									$('#res_msg_'+form).show();
									$('#res_msg_'+form).css({"color":"#ED1326"});
									$('#res_msg_'+form).text(data.msg);

								}
								
					    },
					  	error: function(jqXHR, textStatus, errorThrown) 
				    	{
							$("#multi-msg").text('Sorry,Ajax request failed');
				    	} 	        
				   });
			        
			   }
			   else  /*for older browsers*/
				{
					var  iframeId = 'unique' + (new Date().getTime());
					var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');
					iframe.hide();
					formObj.attr('target',iframeId);
					iframe.appendTo('body');
					iframe.load(function(e)
					{
						var doc = getDoc(iframe[0]);
						var docRoot = doc.body ? doc.body : doc.documentElement;
						var data = docRoot.innerHTML;
						$(".help-block").remove();
	  					 	$(".form-group").removeClass("has-error");
						if(data.result=='error'&&data.type=='validation'){
							$.each(data.msg, function(key, val) {
					   	   		$("#"+key+"_err").remove();
			                    $("#"+key).after("<div class=\"help-block\" id='"+key+"_err'>"+val+"</div>");
			                    $("#"+key).closest(".form-group").addClass("has-error");
			                });
						}
						if(data.result=='success'){
							$('#res_msg').css({"color":"#075617"});
							$('#res_msg').text(data.msg);
						}
						if(data.result=='error'&&data.type=='other'){
							$('#res_msg').css({"color":"#ED1326"});
							$('#res_msg').text(data.msg);
						}
					});
				}
				e.preventDefault();
			});

   	 
	}
	/* IE8 cascading access check */
	function getDoc(frame) {
		     var doc = null;
		     try {
		         if (frame.contentWindow) {
		             doc = frame.contentWindow.document;
		         }
		     } 
		     catch(err) {

		     }
		     if (doc) { 
		         return doc;
		     }
		     try { 
		         doc = frame.contentDocument ? frame.contentDocument : frame.document;
		     } catch(err) {
		         doc = frame.document;
		     }
		     return doc;
	}

	/* To show delete action popup */
	$("#delete-button").click(function(){
		var view='<div class="modal-body" id="del_msg">';
   			view+='<center></center>';
   			view+='Do you want to permanently cancel your account ? This action cannot be reverted!!';
   			view+='</div> ';
   			view+='<div class="modal-footer">';
   			view+='<button type="button" class="btn btn-default" id="disable_btn" data-dismiss="modal">No</button>';
   			view+='<button type="button" class="btn btn-primary" id="del_confirm" data-action="delete">Yes</button></div>';
   			$('#modal').modal('show');
	   		$('#modalContent').html(view);
	   		deleteaction();
	});

	/* To perform delete action */
	function deleteaction(){
   		$('#del_confirm').click(function(){
   			var action=$(this).attr('data-action');
   			$('#disable_btn').attr('disabled',true);
   			$(this).attr('disabled',true);
   			$('.close').attr('disabled',true);
   			$('#del_msg').html('<div style="text-align:center"><img src="../web/images/loader.gif"></div>');
   	 		$.post(user_del_url,{'action':action},function(data){
   	 			$('#del_msg').html('');
   	 			$('#del_msg').css({"color":"#ED1326"});
   	 			if(data.result=='success'){
   	 				$('#del_msg').css({"color":"#075617"});
   	 			}
				 $('#del_msg').text(data.msg);
				 setTimeout(function(){ 
				 	$('#modal').modal('hide'); 
					$.post(user_logout_url,{'action':action},function(data){
						
					},"json");	
					 location.reload();
				 }, 3000);
   	 	 	},"json");
   		});	
   }

});	