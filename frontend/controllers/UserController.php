<?php
namespace frontend\controllers;

use yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\User;
use frontend\models\UserProfile;

/**
 * User controller
 */
class UserController extends Controller
{
	public $errors;
	/**
     * Setting behaviours
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['userupdate', 'profile','updateuserpassword','delete'],
                'rules' => [
                    [
                        'actions' => ['userupdate', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['profile' ,'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['updateuserpassword' ,'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['delete' ,'error'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'profile' => ['get'],
                    'userupdate' => ['post'],
                    'updateuserpassword' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
         return [
             'error' => [
                 'class' => 'yii\web\ErrorAction',
             ],
         ];
    }

	/**
     * Displays user profile page.
     *
     * @return mixed
     */
    public function actionProfile()
    {
        if (\Yii::$app->user->isGuest) {
             return $this->redirect(['site/login']);
        }
        $model = new UserProfile();
        return $this->render('profile',['model'=>$model]);
    }


    /**
     * Updates user avatar
     *
     * @return mixed
     */
    public function actionUseravatar()
    {
        if (\Yii::$app->user->isGuest) {
             return $this->redirect(['site/login']);
        }
        $model = new UserProfile(['scenario' => UserProfile::SCENARIO_USER_AVATAR]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if(!$image){
                return array('result'=>'error','type'=>'other','msg'=>'Please select an image to upload!','img'=>'','details'=>array());
            }
            $model->filename = $image->name;
            $ext = end((explode(".", $image->name)));
            $model->avatar = Yii::$app->security->generateRandomString().".{$ext}";
            $path = Yii::$app->basePath . '/web/uploads/' . $model->avatar;
            if(isset(Yii::$app->user->identity->avatar)){
                $old_file=Yii::$app->user->identity->avatar;
            }
            if($model->update()){
                if($image->saveAs($path)){
                    unlink('../frontend/web/uploads/'.$old_file);
                    return array('result'=>'success','msg'=>'Successfully uploaded!','img'=>$model->avatar,'details'=>array());
                }
            } else {
                return array('result'=>'error','type'=>'other','msg'=>'Unable to upload','img'=>'','details'=>array());
            }
        }
    }

    /**
     * Updates user password
     *
     * @return mixed
     */

    public function actionUpdateuserpassword(){
        if (\Yii::$app->user->isGuest) {
             return $this->redirect(['site/login']);
        }
        $model = new UserProfile(['scenario' => UserProfile::SCENARIO_CHANGE_PWD]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->errors=$this->actionValidate($model);
        if(!empty($this->errors)){
            return array('result'=>'error','type'=>'validation','msg'=>$this->errors,'img'=>'','details'=>array());
        }
        $model->load(Yii::$app->request->post());
        if($model->update()){
            return array('result'=>'success','msg'=>'Successfully changed password!','img'=>'','details'=>array());
        }
        return array('result'=>'error','type'=>'other','msg'=>'Unable to change password','img'=>'','details'=>array());
    }

    /**
     * Perform validation action.
     * @param mixed
     * @return mixed
     */

    public function actionValidate($model){
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    /**
     * Updates user profile
     *
     * @return mixed
     */
    public function actionUserupdate()
    {
        if (\Yii::$app->user->isGuest) {
             return $this->redirect(['site/login']);
        }
        $model = new UserProfile(['scenario' => UserProfile::SCENARIO_USER_UPDATE]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->errors=$this->actionValidate($model);
        if(!empty($this->errors)){
            return array('result'=>'error','type'=>'validation','msg'=>$this->errors,'img'=>'','details'=>array());
        }
        $model->load(Yii::$app->request->post());
        if($model->update()){
            return array('result'=>'success','msg'=>'Successfully Updated!','img'=>'','details'=>array('user_full_name'=>$model->user_full_name,'user_address'=>$model->user_address,'user_age'=>$model->user_age,'contact_no'=>$model->contact_no));
        }
        return array('result'=>'error','type'=>'other','msg'=>'Unable to update','img'=>'','details'=>array());
        
    }

    /**
     * To Cancel user account
     *
     * @return mixed
     */
    public function actionDelete(){
        if (\Yii::$app->user->isGuest) {
             return $this->redirect(['site/login']);
        }
        $model = new UserProfile(['scenario' => UserProfile::SCENARIO_USER_DELETE]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->status=User::STATUS_DELETED;
        if($model->update()){
            return array('result'=>'success','msg'=>'Successfully cancelled! We look forward to serve you again. Thank you!') ;
        }
        return array('result'=>'error','msg'=>'Unable to cancel!. An error occured.');
    }
}	