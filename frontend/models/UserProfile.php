<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use yii\db\ActiveRecord;
use borales\extensions\phoneInput\PhoneInputValidator;
use Yii;

/**
* Class UserProfile
* @package frontend\models
* @property int $id unique UserAvatar identifier
* @property string $name person / user name
* @property array $avatar generated filename on server
* @property string $filename source filename from client
*/

class UserProfile extends Model
{
    /**
    * Scenarios for change profile, avatar, and passwords
    */
    const SCENARIO_CHANGE_PWD = 'changePwd';
    const SCENARIO_USER_UPDATE = 'changeProfile';
    const SCENARIO_USER_AVATAR = 'changeAvatar';
    const SCENARIO_USER_DELETE = 'deleteUser';
    public $user_id;
    public $contact_no;
    public $user_full_name;
    public $user_address;
    public $user_age;
    public $user_gender;
    public $image;
    public $filename;
    public $avatar;
    public $status;
    public $currentpassword;
    public $password;
    public $repeatpassword;

    /**
     * Scenarios for this model
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_USER_UPDATE => ['user_full_name','user_address','contact_no','user_age','user_gender','status'],
            self::SCENARIO_CHANGE_PWD => ['currentpassword','password','repeatpassword'],
            self::SCENARIO_USER_AVATAR => ['image'],
            self::SCENARIO_USER_DELETE => ['status'],
        ];
    }
    /**
     * Rules for this model
     */
    
    public function rules()
    {
        return [
            ['user_full_name', 'filter', 'filter' => 'trim'],
            ['user_full_name', 'required'],

            ['user_address', 'required'],

            [['contact_no'], 'required'],
            [['contact_no'], PhoneInputValidator::className()],

            ['user_gender', 'required'],

            ['user_age', 'required'],
            ['user_age', 'number'],

            [['image'], 'required'],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png','maxSize'=> 1024 * 1024 * 2],

            [['currentpassword', 'password','repeatpassword'],'required','on' => 'changePwd'],
            ['currentpassword', 'comparePasswords', 'on' => 'changePwd'],
            ['repeatpassword','compare','compareAttribute'=>'password'],
                

        ];
    }
    /**
     * Validates old password
     */
    
    public function comparePasswords($attribute, $params)
    {
        if (!Yii::$app->security->validatePassword($this->currentpassword, Yii::$app->user->identity->password_hash))
            $this->addError($attribute, 'Current password is incorrect.');
    }

    /**
     * Updates user
     *
     * @return mixed
     */

    public function update(){
        $user = User::findOne(Yii::$app->user->identity->id);
        if(!empty($this->user_full_name)){
            $user->user_full_name = $this->user_full_name;
        }
        if(!empty($this->user_address)){
            $user->user_address = $this->user_address;
        }
        if(!empty($this->contact_no)){
            $user->contact_no = ltrim($this->contact_no,"+");
            $user->contact_no=str_replace(' ', '', $user->contact_no);
        }
        if(!empty($this->user_age)){
            $user->user_age = $this->user_age;
        }
        if(!empty($this->user_gender)){
            $user->user_gender = $this->user_gender;
        }
        if(!empty($this->filename)){
            $user->filename = $this->filename;
        }
        if(!empty($this->avatar)){
            $user->avatar = $this->avatar;
        }
        if(!empty($this->password)){
            $user->setPassword($this->password);
        }
        if(isset($this->status)){
            $user->status = $this->status;
        }

        return $user->update() ? $user : null;
    } 
}