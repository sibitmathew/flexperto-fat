<?php
use yii\helpers\Html;
use common\models\UserDetails;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

$user_full_name= !empty(Yii::$app->user->identity->user_full_name) ?  Yii::$app->user->identity->user_full_name : " ";
$username= !empty(Yii::$app->user->identity->username) ?  Yii::$app->user->identity->username : " ";
$email= !empty(Yii::$app->user->identity->email) ?  Yii::$app->user->identity->email : " ";
$contact_no= !empty(Yii::$app->user->identity->contact_no) ?  "+".Yii::$app->user->identity->contact_no : "";
$user_address= !empty(Yii::$app->user->identity->user_address) ?  Yii::$app->user->identity->user_address : " ";
$user_age= !empty(Yii::$app->user->identity->user_age) ?  Yii::$app->user->identity->user_age : " ";
$user_gender= !empty(Yii::$app->user->identity->user_gender) ?  Yii::$app->user->identity->user_gender : " ";
$avatar = !empty(Yii::$app->user->identity->avatar) ?  "../web/uploads/".Yii::$app->user->identity->avatar : "../web/images/avatar.png";
 
?>
<div id="w">
    <div id="content" class="clearfix">
      <div id="userphoto"><img src="<?php echo $avatar;?>" width="170" alt="default avatar" id="user_img"></div>
      <h1><span id="user_name"><?php $name=empty(Yii::$app->user->identity->user_full_name) ? $username : $user_full_name; echo $name;?></span></h1>

      <nav id="profiletabs">
        <ul class="clearfix">
          <li><a href="#profile" class="sel">Overview</a></li>
          <li><a href="#edit_profile" class="">Edit Profile</a></li>
          <li><a href="#avatar" class="">Avatar</a></li>
          <li><a href="#settings" class="">Settings</a></li>
        </ul>
      </nav>
      
      <section id="profile" class="">
        <p>Edit user settings:</p>
        
        <p class="setting"><span>Name </span> <span id="user_full_name_view"><?php echo $user_full_name;?></span>&nbsp;</p>

        <p class="setting"><span>Username </span> <span><?php echo $username;?></span>&nbsp;</p>

        <p class="setting"><span>Age </span> <span id="user_age_view"><?php echo $user_age;?></span>&nbsp;</p>

        <p class="setting"><span>Gender </span> <span><?php echo $user_gender;?></span>&nbsp;</p>
        
        <p class="setting"><span>Email </span> <span><?php echo $email;?></span>&nbsp;</p>
        
        <p class="setting"><span>Contact No </span> <span id="contact_no_view"><?php echo $contact_no;?></span>&nbsp;</p>
        
        <p class="setting"><span>Address </span> <span id="user_address_view"><?php echo $user_address;?></span>&nbsp;</p>
        
      </section>
      
      <section id="edit_profile" class="hidden">
      <?php
      use borales\extensions\phoneInput\PhoneInput;
      echo "<h3>Create/Edit Users</h3>";
      echo "<br>";
      echo "<center><span id='res_msg_update-profile'></span></center>";
      $ajax_action= Yii::$app->getUrlManager()->createUrl('user/userupdate');
        $form = ActiveForm::begin([
              'action'=>$ajax_action,
                    'options' => [
                        'id' => 'update-profile',
                        'enctype' => 'multipart/form-data',
                    ],
        ]);
         echo $form->field($model, 'user_full_name')->textInput(array('value'=>$user_full_name))->label('Name');
         
         echo $form->field($model, 'user_address')->textarea(array('value'=>$user_address))->label('Address');
         echo $form->field($model, 'user_age')->textInput(array('value'=>$user_age))->label('Age');
         echo $form->field($model, 'user_gender')->dropDownList(['male' => 'Male', 'female' => 'Female'],
                ['options' =>
                        [                        
                           $user_gender => ['Selected' => 'selected']
                        ]
              ]);
         echo $form->field($model, 'contact_no')->widget(PhoneInput::classname(), [
                    'name' => 'contact_no',
                    'jsOptions' => [
                        'allowExtensions' => true,
                        'nationalMode'=>false],
                    'options'=>[
                        'value'=>$contact_no
                    ]    

                ])->label('Contact No');

         echo '<center>'
         .Html::resetButton('Reset', ['class'=> 'btn btn-default']).'&nbsp;&nbsp;'
         .Html::submitButton('Submit', ['class'=> 'btn btn-primary update-user','data-form'=>'update-profile']).'</center>' ;        
        ActiveForm::end();
    ?>
      </section>
      <section id="avatar" class="hidden">
        <?php 
          echo "<h3>Upload Avatar</h3>";
          echo "<br>";
          echo "<center><span id='res_msg_update-avatar'></span></center>";
          $form = ActiveForm::begin([
                'options'=>['enctype'=>'multipart/form-data',
                'id' => 'update-avatar'], 
                'action'=>Yii::$app->getUrlManager()->createUrl('user/useravatar')
            ]);
            echo $form->field($model, 'image')->widget(FileInput::classname(), [
                'options'=>['accept'=>'image/*'],
                'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],
                'maxFileSize' => '2048',
                'showUpload' => false]
            ]);
            echo Html::submitButton( 'Upload', [
                'class'=>'btn btn-success update-user','data-form'=>'update-avatar']
            );
            ActiveForm::end();
                    ?>
      </section>
      <section id="settings" class="hidden">
      <?php echo "<h3>Account cancellation/Change password</h3>";
      echo Html::button( 'Cancel my account', [
                'class'=>'btn btn-danger', 'id'=>'delete-button']
            );
      echo "<center><span id='res_msg_update-password'></span></center>";
        $form = ActiveForm::begin([
              'action'=>Yii::$app->getUrlManager()->createUrl('user/updateuserpassword'),
                    'options' => [
                        'id' => 'update-password',
                        'enctype' => 'multipart/form-data',
                    ],
        ]);
         echo $form->field($model, 'currentpassword')->passwordInput()->label('Current Password');
         echo $form->field($model, 'password')->passwordInput()->label('New Password');
         echo $form->field($model, 'repeatpassword')->passwordInput()->label('Confirm Password');
         

         echo '<center>'
         .Html::resetButton('Reset', ['class'=> 'btn btn-default']).'&nbsp;&nbsp;'
         .Html::submitButton('Submit', ['class'=> 'btn btn-primary update-user','data-form'=>'update-password']).'</center>' ;        
        ActiveForm::end();
    ?>
      </section>
    </div><!-- @end #content -->
    <style type="text/css">
    .modal-lg{
        width:500px !important;
    }
    </style>
    <?php
        yii\bootstrap\Modal::begin([
            'headerOptions' => ['id' => 'modalHeader'],
            'id' => 'modal',
            'size' => 'modal-lg',
            'clientOptions' => ['backdrop' => 'static', 'keyboard' => TRUE,'closeButton'=>FALSE]
        ]);
        echo "<div id='modalContent'><div style='text-align:center'><img src='../web/images/loader.gif'></div></div>";
        yii\bootstrap\Modal::end();
    ?>
  </div>

