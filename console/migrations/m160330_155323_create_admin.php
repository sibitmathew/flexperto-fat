<?php

use yii\db\Schema;
use yii\db\Migration;

class m160330_155323_create_admin extends Migration
{
    public function up()
    {
    	$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%admin}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'admin_full_name' => $this->text()->notNull(),
            'admin_contact' => $this->string(32)->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert('admin',array(
             'email'=>'admin@admin.com',
             'username' =>'admin',
             'admin_full_name'=> 'Admin - Default',
             'admin_contact'=>'123456789',
             'auth_key' => '5Ui7810Bs84ahduPmhLJAAdBXZZ5vX_a',
             'password_hash'=>'$2y$13$WhE2RTVTY9VfTSFxhq93/ul0xvaAvNFvE1LPPF2epBlRmGZu8hHy.',
             'status'=>'10',
             'created_at'=>'1458588430',
             'updated_at'=>'1458588430'

        ));
    }

    public function down()
    {
        echo "m160330_155323_create_admin cannot be reverted.\n";

        return false;
    }
}
