<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use yii\db\Query;
use yii\web\UploadedFile;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * Signup form
 */
class UserDetailsFrontEnd extends Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    public $user_id;
    public $username;
    public $email;
    public $contact_no;
    public $password;
    public $repeatpassword;
    public $user_full_name;
    public $user_address;
    public $filename;
    public $avatar;
    public $image;
    public $status;
    public $new;
    public $action;
    public $user_age;
    public $user_gender;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['username', 'password','repeatpassword','user_full_name','user_address','user_age','user_gender','contact_no','email','status'],
            self::SCENARIO_UPDATE => ['user_full_name','user_address','user_age','user_gender','contact_no','status','new'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['repeatpassword', 'required'],
            ['repeatpassword', 'compare','compareAttribute'=>'password','message'=>"Passwords don't match",'on'=>'create'],

            ['user_full_name', 'filter', 'filter' => 'trim'],
            ['user_full_name', 'required'],

            ['user_address', 'required'],

            ['user_gender', 'required'],

            ['user_age', 'required'],
            ['user_age', 'number'],

            [['contact_no'], 'required','on' => 'create'],
            [['contact_no'], PhoneInputValidator::className()],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png','maxSize'=>'0'],
        ];
    }
     /**
     * Get total active user count.
     *
     * @return Integer
     */
    public function getcount($status=null)
    {
        if($status == 'active'){
            return User::find()
                ->where(['status' => User::STATUS_ACTIVE])
                ->count();
        }
        else if($status == 'inactive'){
             return User::find()
                ->where(['status' => User::STATUS_INACTIVE])
                ->count();
        }
        else if($status == 'new'){
             return User::find()
                ->where(['new' => '1'])
                ->andWhere(['!=','status' , User::STATUS_DELETED])
                ->count();
        }
        else{
            return User::find()
                ->where(['!=','status' , User::STATUS_DELETED])
                ->count();
        }
        
    }
    /**
     * Get total active user count.
     *
     * @return Array
     */
    public function getuserlist()
    {
        $query = new Query;
        $query  ->select(['user.id','user.username',
            'user.email',
            'user.user_full_name',
            'user.user_address',
            'user.status',
            'user.new']) 
                ->from('user')
                ->where(['!=','user.status',User::STATUS_DELETED])
                ->orderBy('id DESC');        
        $command = $query->createCommand();
        return $command->queryAll();   
    }
    /**
     * Get total active user count.
     *
     * @return Array
     */
    public function getuserdetails($user_id){
        $query = new Query;
        $query  ->select(['user.id','user.username',
            'user.email',
            'user.status',
            'user.user_full_name',
            'user.user_address',
            'user.avatar',
            'user.user_age',
            'user.user_gender',
            'user.contact_no'])  
                ->from('user')
                ->where(['user.id'=>$user_id])
                ->andWhere(['!=','user.status', User::STATUS_DELETED]);         
        $command = $query->createCommand();
        return $command->queryOne(); 
    }  

    public function create(){

        $user = new User();
        if(!empty($this->username)){
            $user->username = $this->username;
        }
        if(!empty($this->password)){
            $user->setPassword($this->password);
        }
        if(!empty($this->email)){
            $user->email = $this->email;
        }
        if(!empty($this->user_full_name)){
            $user->user_full_name = $this->user_full_name;
        }
        if(!empty($this->user_address)){
            $user->user_address = $this->user_address;
        }
        if(!empty($this->user_age)){
            $user->user_age = $this->user_age;
        }
        if(!empty($this->user_gender)){
            $user->user_gender = $this->user_gender;
        }
        if(!empty($this->contact_no)){
            $user->contact_no = ltrim($this->contact_no,"+");
            $user->contact_no=str_replace(' ', '', $user->contact_no);
        }
        if(!empty($this->filename)){
            $user->filename = $this->filename;
        }
        if(!empty($this->avatar)){
            $user->avatar = $this->avatar;
        }
        if(!empty($this->status)){
            $user->status = $this->status;
        }
        $user->generateAuthKey();
        return $user->save() ? $user : null;
    } 

    public function update(){
        $user = User::findOne($this->user_id);
        if(!empty($this->user_full_name)){
            $user->user_full_name = $this->user_full_name;
        }
        if(!empty($this->user_address)){
            $user->user_address = $this->user_address;
        }
        if(!empty($this->contact_no)){
            $user->contact_no = ltrim($this->contact_no,"+");
            $user->contact_no=str_replace(' ', '', $user->contact_no);
        }
        if(!empty($this->user_age)){
            $user->user_age = $this->user_age;
        }
        if(!empty($this->user_gender)){
            $user->user_gender = $this->user_gender;
        }
        if(!empty($this->filename)){
            $user->filename = $this->filename;
        }
        if(!empty($this->avatar)){
            $user->avatar = $this->avatar;
        }
        if(!empty($this->status)){
            $user->status = $this->status;
        }
        if(!empty($this->new)){
            $user->new = $this->new;
        }
        return $user->update() ? $user : null;
    } 
    public function doaction(){
        $user = User::findOne($this->user_id);
        if($this->action == 'delete'){
            $user->status=User::STATUS_DELETED;
        }
        if($this->action == 'deactivate'){
            $user->status=User::STATUS_INACTIVE;
        }
        if($this->action == 'activate'){
            $user->status=User::STATUS_ACTIVE;
        }
        return $user->update() ? $user : null;
    }    
    
}	