<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\AdminLoginForm;
use common\models\User;
use backend\models\UserDetailsFrontEnd;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\helpers\Url;


/**
 * Admin controller for main admin actions
 */
class AdminController extends Controller
{
    /**
     * Required variables
     */
    public $errors;
    public $path;
    public $filename;
    public $avatar;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['dashboard', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['getusers', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['viewusers', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['editusers', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['createuser', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['updateuser', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['changeuserstatus', 'error'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'dashboard' => ['post'],
                    'getusers' => ['post'],
                    'editusers' => ['post'],
                    'viewusers' => ['post'],
                    'createuser' => ['post'],
                    'updateuser' => ['post'],
                    'deleteuser' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
         return [
             'error' => [
                 'class' => 'yii\web\ErrorAction',
             ],
         ];
    }

    /*
    * Function to get back end dashboard and its values
    *  @return mixed
    */

    public function actionDashboard()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
            return $this->redirect(['site/login']);
        }
         $model=new UserDetailsFrontEnd();
         Yii::$app->response->format = Response::FORMAT_JSON;
         $active_users = $model->getcount('active');
         $non_active_users = $model->getcount('inactive');
         $total_users = $model->getcount('total');
         $new_users = $model->getcount('new');
         return array('view' => $this->renderAjax('default',array('active_users' => $active_users,'non_active_users'=>$non_active_users,'total_users'=>$total_users,'new_users'=>$new_users),true));
    }

    /*
    * Function to get front end users list to display
    *  @return mixed
    */

    public function actionGetusers()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
            return $this->redirect(['site/login']);
        }
         $model=new UserDetailsFrontEnd();
         Yii::$app->response->format = Response::FORMAT_JSON;
         $ul = $model->getuserlist();
         return array('view' => $this->renderAjax('users',array('user_list'=>$ul),true));
    }

    /*
    * Function to get front end user to edit
    *  @return mixed
    */

    public function actionEditusers()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
            return $this->redirect(['site/login']);
        }
         $us=array();
         $model=new UserDetailsFrontEnd(['scenario' => UserDetailsFrontEnd::SCENARIO_UPDATE]);
         Yii::$app->response->format = Response::FORMAT_JSON;
         if(Yii::$app->request->post('user_id') !=''){
            $us = $model->getuserdetails(Yii::$app->request->post('user_id'));
            $model->user_id=Yii::$app->request->post('user_id');
            $model->new = 'no';
            $model->update();
         }
         return array('view' => $this->renderAjax('userEditView',array('user'=>$us,'model'=>$model),true));
    }

    /*
    * Function to get front end user to view
    *  @return mixed
    */

    public function actionViewusers()
    {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
            return $this->redirect(['site/login']);
        }
         $us=array();
         $model=new UserDetailsFrontEnd(['scenario' => UserDetailsFrontEnd::SCENARIO_UPDATE]);
         Yii::$app->response->format = Response::FORMAT_JSON;
         if(Yii::$app->request->post('user_id') !=''){
            $us = $model->getuserdetails(Yii::$app->request->post('user_id'));
            $model->user_id=Yii::$app->request->post('user_id');
            $model->new = 'no';
            $model->update();
         }
         return array('view' => $this->renderAjax('userOverView',array('user'=>$us),true));
    }

    /*
    * Function to validate each user input based on scenarios
    *  @param mixed 
    *  @return mixed
    */

    public function actionValidate($model){
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    /*
    * Function to upload user avatar from admin
    *  @param mixed 
    *  @return bool
    */

    public function actionAvatar($model){
        if(Yii::$app->request->post()!=''){
            $image = UploadedFile::getInstance($model, 'image');
            if (isset($image)) {
                $model->filename = $image->name;
                $ext = end((explode(".", $image->name)));
                $model->avatar = Yii::$app->security->generateRandomString().".{$ext}";
                $path = Yii::getAlias('@frontend') . '/web/uploads/' . $model->avatar;
                if($image->saveAs($path)){
                    return true;
                }
            }            
            return false;
        }
    }

    /*
    * Function to create a front end user
    *  @return mixed
    */

    public function actionCreateuser() {
        if (\Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
            return $this->redirect(['site/login']);
        }
        $model = new UserDetailsFrontEnd(['scenario' => UserDetailsFrontEnd::SCENARIO_CREATE]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->errors=$this->actionValidate($model);
        if(!empty($this->errors)){
            return array('result'=>'error','type'=>'validation','msg'=>$this->errors);
        }
        $model->load(Yii::$app->request->post());
        $this->actionAvatar($model);
        if($model->create()){
            return array('result'=>'success','msg'=>'Successfully Created');
        }
        return array('result'=>'error','type'=>'other','msg'=>'Unable to save');
    }

    /*
    * Function to update a front end user
    *  @return mixed
    */

    public function actionUpdateuser(){
        if (\Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
            return $this->redirect(['site/login']);
        }
        $old_file="";
        $model = new UserDetailsFrontEnd(['scenario' => UserDetailsFrontEnd::SCENARIO_UPDATE]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $this->errors=$this->actionValidate($model);
        if(!empty($this->errors)){
            return array('result'=>'error','type'=>'validation','msg'=>$this->errors);
        }
        $model->load(Yii::$app->request->post());
        $model->user_id=Yii::$app->request->post('user_id');
        if($this->actionAvatar($model)){
            $user=User::findOne($model->user_id);
            if(isset($user->avatar)){
                $old_file=$user->avatar;
            }
        }
        if($model->update()){
            @unlink('../frontend/web/uploads/'.$old_file);
            return array('result'=>'success','msg'=>'Successfully Updated!');
        }
        return array('result'=>'error','type'=>'other','msg'=>'Unable to update');
    }

    /*
    * Function to get change users status
    *  @return mixed
    */

    public function actionChangeuserstatus(){
        if (\Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
            return $this->redirect(['site/login']);
        }
        $model = new UserDetailsFrontEnd();
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model->user_id = Yii::$app->request->post('user_id');
        $model->action = Yii::$app->request->post('action');
        if($model->doaction()){
            return array('result'=>'success','msg'=>'Successfully deleted/changed!') ;
        }
        return array('result'=>'error','msg'=>'Unable to delete/change!');
    }
}