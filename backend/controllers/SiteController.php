<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\AdminLoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
         return [
             'error' => [
                 'class' => 'yii\web\ErrorAction',
             ],
         ];
    }

    /*
    * Function to display index view
    *  @return mixed
    */

    public function actionIndex()
    {
        return $this->render('index');
    }

    /*
    * Function to perform admin login action
    *  @return mixed
    */
     public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new AdminLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'common';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /*
    * Function to perform admin logout action
    *  @return mixed
    */

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
