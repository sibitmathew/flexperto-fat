<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'web/css/site.css',
        'web/css/custom-styles.css',
        'web/css/font-awesome.css',
        'http://fonts.googleapis.com/css?family=Open+Sans',
        'web/js/dataTables/dataTables.bootstrap.css'
    ];
    public $js = [
        'web/js/jquery.metisMenu.js',
        'web/js/dataTables/jquery.dataTables.js',
        'web/js/dataTables/dataTables.bootstrap.js',
        'web/js/admin.js',
        'web/js/custom-scripts.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
