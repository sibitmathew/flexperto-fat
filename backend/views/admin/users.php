<div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Users list
                             <div style="position:relative;">
                                <button class="btn btn-success refresh_table"> Refresh</button>
                             </div>
                             <div style="position:relative;margin-left:84%;">
                                <button class="btn btn-success edit_user"  data-id=""><i class="fa fa-edit "></i> Create new user</button>
                             </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>SI.No</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Activation</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   <?php $count=1;
                                   foreach ($user_list as $user) {
                                        $new="";
                                        if($user['new']=='1'){
                                            $new="success";
                                        }
                                    ?>     
                                        <tr class="<?php $class=$count%2 ? 'odd' : 'even'; echo $class;?>  gradeX <?php echo $new;?>" id="row_id_<?=$user['id'];?>">
                                            <td><?=$count;?></td>
                                            <td><?=$user['user_full_name'];?></td>
                                            <td><?=$user['username'];?></td>
                                            <td><?=$user['email'];?></td>
                                            <td class="center">
                                                <?php if($user['status'] ==10){ ?>
                                                    <a href="javascript:void(0);" title="Turn off activation" class="btn btn-success btn-sm btn-status" id="action_<?=$user['id'];?>" data-status="deactivate" data-id="<?=$user['id'];?>">On</a></td>
                                                <?php }else{
                                                ?>
                                                    <a href="javascript:void(0);" title="Turn on activation" class="btn btn-danger btn-sm btn-status" id="action_<?=$user['id'];?>" data-status="activate" data-id="<?=$user['id'];?>">Off</a></td>
                                                <?php }?>
                                                
                                            <td class="center">
                                                <button class="btn btn-default view_user" title="View this user profile" data-id="<?=$user['id'];?>"><i class="fa fa-pencil"></i> View</button>
                                                <button class="btn btn-primary edit_user" title="Edit this user profile" data-id="<?=$user['id'];?>"><i class="fa fa-edit "></i> Edit</button>
                                                <button class="btn btn-danger del_user" title="Delete this user profile" data-id="<?=$user['id'];?>"><i class="fa fa-pencil"></i> Delete</button>
                                            </td>
                                        </tr>
                                   <?php  ++$count;} ?>    
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>

                

                <script>
                        $(document).ready(function () {
                            $('#dataTables-example').dataTable();
                        });
                </script>
