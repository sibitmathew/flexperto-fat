<?php
    use yii\helpers\Html;
	echo "<center><h3>User Overview</h3></center>";
    $avatar=isset($user['avatar'])? $user['avatar'] : '';
    if($avatar !=''){
        echo "<div id='userphoto'><img src='../frontend/web/uploads/".$avatar."' width='170' alt='avatar'></div>";
    }
	echo "<br>";
    $user_full_name=isset($user['user_full_name'])? $user['user_full_name'] : '';
    $username=isset($user['username'])? $user['username'] : '';
    $user_address=isset($user['user_address'])? $user['user_address'] : '';
    $contact_no=isset($user['contact_no'])? '+'.$user['contact_no'] : '';
    $email=isset($user['email'])? $user['email'] : '';
    $status=isset($user['status'])? $user['status'] : '';
?>
    <p class="setting"><span>Name </span> <?php echo $user_full_name;?>&nbsp;</p>
    <p class="setting"><span>Username </span> <?php echo $username;?>&nbsp;</p>
    <p class="setting"><span>Email </span> <?php echo $email;?>&nbsp;</p>
    <p class="setting"><span>Contact No </span> <?php echo $contact_no;?>&nbsp;</p>
    <p class="setting"><span>Address </span> <?php echo $user_address;?>&nbsp;</p>
    <p class="setting"><span>Status </span> <?php if($status=='10'){echo "Active";}else{echo "Not Active";}?>&nbsp;</p>
<?php
    echo '<center>'.Html::button('Cancel', ['class'=> 'btn btn-default','data-dismiss'=>'modal']).'&nbsp;&nbsp;</center>' ; 
?>