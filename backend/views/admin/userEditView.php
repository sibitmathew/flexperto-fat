<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
    use yii\web\Application;
	use kartik\file\FileInput;
    use backend\assets\AppAsset;
	use borales\extensions\phoneInput\PhoneInput;

    AppAsset::register($this);
	echo "<center><h3>Create/Edit Users</h3></center>";
    $avatar=isset($user['avatar'])? $user['avatar'] : '';
    if($avatar !=''){
        echo "<div id='userphoto'><img src='../frontend/web/uploads/".$avatar."' width='170' alt='avatar'></div>";
    }
	echo "<br>";
	echo "<center><span id='res_msg'></span></center>";
	$ajax_action=isset($user['id']) ? Yii::$app->getUrlManager()->createUrl('admin/updateuser') : Yii::$app->getUrlManager()->createUrl('admin/createuser');
    $form = ActiveForm::begin([
    			'action'=>$ajax_action,
                'options' => [
                    'id' => 'update-user',
                    'enctype' => 'multipart/form-data',
                ],
    ]);
    $user['id']=isset($user['id'])? $user['id'] : '';
    $user['user_full_name']=isset($user['user_full_name'])? $user['user_full_name'] : '';
    $user['user_address']=isset($user['user_address'])? $user['user_address'] : '';
    $user['contact_no']=isset($user['contact_no'])? $user['contact_no'] : '';
    $user['email']=isset($user['email'])? $user['email'] : '';
    $user['status']=isset($user['status'])? $user['status'] : '';
    $user['user_age']=isset($user['user_age'])? $user['user_age'] : '';
    $user['user_gender']=isset($user['user_gender'])? $user['user_gender'] : '';
     echo Html::hiddenInput('user_id', $user['id']);
     echo $form->field($model, 'user_full_name')->textInput(array('value'=>$user['user_full_name']))->label('Name');
     if($user['id'] == ''){
     	$user['username']=isset($user['username'])? $user['username'] : '';
     	echo $form->field($model, 'username')->textInput(array('value'=>$user['username']))->label('Username');
     	echo $form->field($model, 'password')->passwordInput()->label('Password');
     	echo $form->field($model, 'repeatpassword')->passwordInput()->label('Repeat Password');
     	echo $form->field($model, 'email')->textInput(array('value'=>$user['email']))->label('Email');
     }
     echo $form->field($model, 'user_age')->textInput(array('value'=>$user['user_age']))->label('Age');
     echo $form->field($model, 'user_gender')->dropDownList(['male' => 'Male', 'female' => 'Female'],
            ['options' =>
                    [                        
                       $user['user_gender'] => ['Selected' => 'selected']
                    ]
          ]);
     echo $form->field($model, 'user_address')->textarea(array('value'=>$user['user_address']))->label('Address');
     echo $form->field($model, 'status')->dropDownList(['10' => 'Active', '20' => 'Inactive'],
     		['options' =>
                    [                        
                       $user['status'] => ['Selected' => 'selected']
                    ]
          ]);
     if($user['contact_no'] != ''){
        $user['contact_no']= '+'.$user['contact_no'];
     }   
     echo $form->field($model, 'contact_no')->widget(PhoneInput::classname(), [
                'name' => 'contact_no',
     			'jsOptions' => [
         			'allowExtensions' => true,
         			'nationalMode'=>false,
                    ],
                'options'=>[
                'value'=>$user['contact_no']
                ]    
            ])->label('Contact No');
     echo $form->field($model, 'image')->widget(FileInput::classname(), [
     			'name' => 'filename',
                'options'=>['accept'=>'image/*'],
                'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],
                'maxFileSize' => '2048',
                'showUpload' => false],
            ]);
     echo '<center>'
     .Html::button('Cancel', ['class'=> 'btn btn-default','data-dismiss'=>'modal']).'&nbsp;&nbsp;'
     .Html::submitButton('Submit', ['class'=> 'btn btn-primary','id'=>'submit-user']).'</center>' ;        
    ActiveForm::end();
    

    ?>