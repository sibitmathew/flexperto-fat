<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use nirvana\showloading\ShowLoadingAsset;

ShowLoadingAsset::register($this);
AppAsset::register($this);
 $this->title = 'CRUD-FAT-BACKEND'; 
 $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script type="text/javascript">
        var default_url="<?php echo \Yii::$app->getUrlManager()->createUrl('admin/dashboard') ?>";
        var user_view_url="<?php echo \Yii::$app->getUrlManager()->createUrl('admin/getusers') ?>";
        var user_overview_url="<?php echo \Yii::$app->getUrlManager()->createUrl('admin/viewusers') ?>";
        var user_edit_url="<?php echo \Yii::$app->getUrlManager()->createUrl('admin/editusers') ?>";
        var user_action_url="<?php echo \Yii::$app->getUrlManager()->createUrl('admin/changeuserstatus') ?>";
    </script>
    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="javascript:void(0);"><i class="fa fa-gear"></i> <strong>CRUD-FAT-ADMIN</strong></a>
            </div>  
             <ul class="nav navbar-top-links navbar-right"> 
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                         <li>
                            <?php
                               echo Html::beginForm(['/site/logout'], 'post')
                              . Html::submitButton(
                                'Logout',
                                ['class' => 'fa fa-sign-out fa-fw']
                            )
                            . Html::endForm();
                            ?>    
                        </li> 
                       
                           
                    </ul>
                     
                </li>
                <!-- /.dropdown -->
            </ul> 
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
		<div id="sideNav" href=""><i class="fa fa-caret-right"></i></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a class="active-menu l-menu" href="javascript:void(0);" id="show_dash"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a class="l-menu" href="javascript:void(0);" id="show_user"><i class="fa fa-user"></i> Users</a>
                    </li>
					


                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                             <small>Dashboard</small>
                        </h1>
						<ol class="breadcrumb">
                          <li><a href="javascript:void(0);">Admin</a></li>
                          <li class="active"><a href="javascript:void(0);">Dashboard</a></li>
                        </ol>
                    </div>
                </div>
				
				
                <!-- /. ROW  -->
            <div id="temp_view">
                
            </div>
		
				<footer>
				
        
				</footer>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <style type="text/css">
    .modal-lg{
        width:500px !important;
    }
    </style>
    <?php
                    yii\bootstrap\Modal::begin([
                        'headerOptions' => ['id' => 'modalHeader'],
                        'id' => 'modal',
                        'size' => 'modal-lg',
                        'clientOptions' => ['backdrop' => 'static', 'keyboard' => TRUE]
                    ]);
                    echo "<div id='modalContent'><div style='text-align:center'><img src='web/img/loader.gif'></div></div>";
                    yii\bootstrap\Modal::end();
                ?>
<?php $this->endBody() ?>

</body>

</html>
<?php $this->endPage() ?>