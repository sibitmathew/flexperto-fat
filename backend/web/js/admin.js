
 $(document).ready(function(){
   loaddeafult();
   $('#show_dash').click(function(){
   		loaddeafult();
   });
   $('#show_user').click(function(){
   		loadusers();
   });
   
   
   // Get the default dash board view on load
   function loaddeafult(){
       $('#temp_view').showLoading();
       $.post(default_url,{'dashboard': 'true'},function(data){
            $('#temp_view').hideLoading();
            $("#temp_view").html(data.view);
            $('.l-menu').removeClass("active-menu");
            $('#show_dash').addClass("active-menu");
       },"json");
   }
   // Get the user view 
   function loadusers(){
       $('#temp_view').showLoading();
       $.post(user_view_url,{'user': 'true'},function(data){
            $('#temp_view').hideLoading();
            $("#temp_view").html(data.view);
            $('.l-menu').removeClass("active-menu");
            $('#show_user').addClass("active-menu");
            viewusermodal();
            editusermodal();
            showusermodaldelete();
            doActivation();
            refresh_table();
       },"json");
   }
   function refresh_table(){
   		$('.refresh_table').click(function(){
   			loadusers();
   		});
   }
   function showusermodaldelete(){
   		$('.del_user').click(function(){
   			var user_id=$(this).attr('data-id');
   			var view='<div class="modal-body">';
   			view+='<center><span id="del_msg"></span></center>';
   			view+='Do you want to permanently delete this user ? This action cannot be reverted!!';
   			view+='</div> ';
   			view+='<div class="modal-footer">';
   			view+='<button type="button" class="btn btn-default" data-dismiss="modal">No</button>';
   			view+='<button type="button" class="btn btn-primary del_confirm" data-id="'+user_id+'" data-status="delete">Yes</button></div>';
   			$('#modal').modal('show');
	   		$('#modalContent').html(view);
	   		deleteaction();
   		});
   }
   function deleteaction(){
   		$('.del_confirm').click(function(){
   			var user_id=$(this).attr('data-id');
   			var action=$(this).attr('data-status');
   			$.post(user_action_url,{'user_id':user_id,'action':action},function(data){
   				$('#res_msg').css({"color":"#ED1326"});
   				if(data.result=='success'){
   					$('#del_msg').css({"color":"#075617"});
   				}
				$('#del_msg').text(data.msg);
				setTimeout(function(){ 
					$('#modal').modal('hide'); 
					loadusers();
				}, 1000);
   		 	},"json");
   		});	
   }
   function viewusermodal(){
   		$('.view_user').click(function(){
   			var user_id=$(this).attr('data-id');
   			$.post(user_overview_url,{'user': 'true','user_id':user_id},function(data){
	   		 	$('#modal').modal('show');
	   		 	$('#modalContent').html(data.view);
	   		 	$('#row_id_'+user_id).removeClass('success');
	   		 	saveuser();
   		 	},"json");
   		});
   }
   function editusermodal(){
   		$('.edit_user').click(function(){
   			var user_id=$(this).attr('data-id');
   			$.post(user_edit_url,{'user': 'true','user_id':user_id},function(data){
	   		 	$('#modal').modal('show');
	   		 	$('#modalContent').html(data.view);
	   		 	$('#row_id_'+user_id).removeClass('success');
	   		 	saveuser();
   		 	},"json");
   		});
   }
   function saveuser(){
   		
   		$(document).off('submit', '#update-user').on('submit', '#update-user', function(e){
			var formObj = $(this);
			var formURL = formObj.attr("action");

			if(window.FormData !== undefined)  // for HTML5 browsers
				{
					var formData = new FormData(this);
					$.ajax({
			        	url: formURL,
				        type: 'POST',
						data:  formData,
						mimeType:"multipart/form-data",
						contentType: false,
			    	    cache: false,
			        	processData:false,
			        	dataType: 'json',
						success: function(data, textStatus, jqXHR)
					    {
								$(".help-block").remove();
	   	  					 	$(".form-group").removeClass("has-error");
								if(data.result=='error'&&data.type=='validation'){
									$.each(data.msg, function(key, val) {
							   	   		$("#"+key+"_err").remove();
					                    $("#"+key).after("<div class=\"help-block\" id='"+key+"_err'>"+val+"</div>");
					                    $("#"+key).closest(".form-group").addClass("has-error");
					                });
								}
								if(data.result=='success'){
									$("#modal").animate({ scrollTop: 0 }, "fast");
									$('#res_msg').css({"color":"#075617"});
									$('#res_msg').text(data.msg);
									setTimeout(function(){ 
										$('#modal').modal('hide'); 
										loadusers();
									}, 1000);
								}
								if(data.result=='error'&&data.type=='other'){
									$('#res_msg').css({"color":"#ED1326"});
									$('#res_msg').text(data.msg);
								}
								
					    },
					  	error: function(jqXHR, textStatus, errorThrown) 
				    	{
							$("#multi-msg").text('AJAX Request Failed');
				    	} 	        
				   });
			        
			   }
			   else  //for olden browsers
				{
					var  iframeId = 'unique' + (new Date().getTime());
					var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');
					iframe.hide();
					formObj.attr('target',iframeId);
					iframe.appendTo('body');
					iframe.load(function(e)
					{
						var doc = getDoc(iframe[0]);
						var docRoot = doc.body ? doc.body : doc.documentElement;
						var data = docRoot.innerHTML;
						$(".help-block").remove();
	  					 	$(".form-group").removeClass("has-error");
						if(data.result=='error'&&data.type=='validation'){
							$.each(data.msg, function(key, val) {
					   	   		$("#"+key+"_err").remove();
			                    $("#"+key).after("<div class=\"help-block\" id='"+key+"_err'>"+val+"</div>");
			                    $("#"+key).closest(".form-group").addClass("has-error");
			                });
						}
						if(data.result=='success'){
							$('#res_msg').css({"color":"#075617"});
							$('#res_msg').text(data.msg);
						}
						if(data.result=='error'&&data.type=='other'){
							$('#res_msg').css({"color":"#ED1326"});
							$('#res_msg').text(data.msg);
						}
					});
					}
					e.preventDefault();
				});

   	 
	}



	    function getDoc(frame) {
		     var doc = null;
		     // IE8 cascading access check
		     try {
		         if (frame.contentWindow) {
		             doc = frame.contentWindow.document;
		         }
		     } 
		     catch(err) {

		     }
		     if (doc) { 
		         return doc;
		     }
		     try { 
		         doc = frame.contentDocument ? frame.contentDocument : frame.document;
		     } catch(err) {
		         doc = frame.document;
		     }
		     return doc;
		}

		function doActivation(){
			$('.btn-status').click(function(){
   			var user_id=$(this).attr('data-id');
   			var action=$(this).attr('data-status');
   			$.post(user_action_url,{'user_id':user_id,'action':action},function(data){
   				if(data.result=='success'){
   					if(action == 'deactivate'){
   						$('#action_'+user_id).removeClass('btn-success');
   						$('#action_'+user_id).addClass('btn-danger');
   						$('#action_'+user_id).attr('data-status','activate');
   						$('#action_'+user_id).text('Off');
   						$('#action_'+user_id).attr('title','Turn on activation');
   					}
   					if(action == 'activate'){
   						$('#action_'+user_id).removeClass('btn-danger');
   						$('#action_'+user_id).addClass('btn-success');
   						$('#action_'+user_id).attr('data-status','deactivate');
   						$('#action_'+user_id).text('On');
   						$('#action_'+user_id).attr('title','Turn off activation');
   					}
   				}

   		 	},"json");
   		});	
		}

 });